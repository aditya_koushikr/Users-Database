export interface UsersModal {
    name: string;
    position: string;
    office: string;
    age: number;
    date: string;
    salary: string;
}