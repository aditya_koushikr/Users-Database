import { NgModule } from "@angular/core";
import { HomeRoutingModule } from "./home-routing.module";
import { CommonModule } from "@angular/common";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { HomeComponent } from "./home.component";
import { UserPipe } from "./home.pipe";
import { HomeService } from "./home.service";

@NgModule({
    imports: [HomeRoutingModule, CommonModule, NgbModule.forRoot()],
    declarations: [
        HomeComponent,
        UserPipe
    ],
    providers: [HomeService]
})
export class HomeModule {

}