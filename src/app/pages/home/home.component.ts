import { Component, OnInit } from '@angular/core';
import { UsersModal } from './users.modal';
import { HomeService } from './home.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  private dataSource: string = 'assets/users.json';         // contains path of data source
  public usersList: Array<UsersModal> = [];               // contains users's information
  public sortType: number = 1;                            // used to sort data by ascending order or descending order
  public sortField: string = 'name';
  constructor(private homeService: HomeService) {
    this.getUsersList();            // gets user's list
  }

  ngOnInit() {
  }

  // called from constructor to get user's list
  private getUsersList(): void {
    this.homeService.getUsersList(this.dataSource).subscribe((res: Array<UsersModal>) => {
      this.usersList = res;
    }, (error) => {
      console.log(error.error.message)
    });
  }

  // sorts users based on field selected
  public sortByField(field) {
    this.sortField = field.target.value;
  }

}
