import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class HomeService {
    constructor(private http: HttpClient) {}
    // gets users list 
    public getUsersList(url: string) {
        return this.http.get(url);
    }
}