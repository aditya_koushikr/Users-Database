import { PipeTransform, Pipe } from "@angular/core";
import { UsersModal } from "./users.modal";

@Pipe({
    name: 'sortBy'
})
export class UserPipe implements PipeTransform {
    transform(users: Array<UsersModal>, field: string, sortType: number): Array<UsersModal> {
        if (users.length > 0) {
            users.sort((a: UsersModal, b: UsersModal) => {
                if (a[field] < b[field]) {
                    return -1 * sortType;
                } else if (a[field] > b[field]) {
                    return 1 * sortType;
                } else {
                    return 0;
                }
            });
            return users;
        }
    }
}